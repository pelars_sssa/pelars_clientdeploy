#!/bin/bash

echo "system update.."

sudo cp `pwd`/../kinect_rules/90-kinect2.rules /etc/udev/rules.d 
sudo add-apt-repository -y ppa:gstreamer-developers/ppa
sudo apt-get update 
sudo apt-get upgrade -y
sudo apt-get install docker.io screen build-essential
sudo apt-get update
sudo apt-get install -y gstreamer* x264 libx264-dev
sudo apt-get install vlc

echo "system update complete"
echo "nvidia driver and cuda installation.."
screen sudo ./driver.sh

